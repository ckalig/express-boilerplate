# TypeScript Express Boilerplate

This project is a boilerplate for Node.js Express API's using TypeScript

### Setup ###

* Clone this repository
* cd express-boilerplate
* npm install
* npm run grunt
* npm start

For suggestions or questions just email me: <info@ckalig.de>