import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import * as config from "../cfg/config.js";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");

// Routers
import BaseRouter from "./routes/BaseRouter";

/**
 * The server.
 *
 * @class Server
 */
export class Server {

  public app: express.Application;
  public cfg;

  /**
   * Bootstrap the application.
   *
   * @class Server
   * @method bootstrap
   * @static
   * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
   */
  public static bootstrap(): Server {
    return new Server();
  }

  /**
   * Constructor.
   *
   * @class Server
   * @constructor
   */
  constructor() {
    //create expressjs application
    this.app = express();

    //configure application
    this.config();

    //add routes
    this.routes();

    //add api
    this.api();
  }

  /**
   * Create REST API routes
   *
   * @class Server
   * @method api
   */
  public api() {
    //empty for now
    this.app.use(BaseRouter);
  }

  /**
   * Configure application
   *
   * @class Server
   * @method config
   */
  public config() {
    this.cfg = config;

    // register middleware functions
    this.app.use(logger("dev"));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({
      extended: true
    }));
    this.app.use(cookieParser(this.cfg.COOKIE_SECRET));
    this.app.use(methodOverride());
  }

  /**
   * Create router
   *
   * @class Server
   * @method api
   */
  public routes() {
    //empty for now
  }
}