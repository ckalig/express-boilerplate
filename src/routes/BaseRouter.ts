import {Router, Request, Response, NextFunction} from "express";

class BaseRouter {

    router: Router;
        
    constructor() {
        this.router = Router();
        this.init();
    }

    private base(req: Request, res: Response, next: NextFunction) {
        return res.status(200).json("Node Express TypeScript Rest API");
    }

    private init() {
        this.router.get("/", this.base);
    }

}

export default new BaseRouter().router;